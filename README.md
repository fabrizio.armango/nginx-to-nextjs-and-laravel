# nginx-phpfpm

### Routing Behaviour
- ENDPOINT -> NextJS frontend application
- ENDPOINT/ -> NextJS frontend application
- ENDPOINT/api -> Laravel API for partners
- ENDPOINT/api/web -> Laravel API for frontend


#### Notes
> Both web and php containers need the folder containing php code. The web container uses it invoking try_files to check if files exist, and then the php container to read the code to be executed in the SCRIPT_FILENAME.

- The php files are stored inside the folder `/php-code` of the `php` and `web` containers.
- The frontend is located in `/var/www/frontend` of `web` container.
